// import java.util.Scanner;
public class Calculator {
  public static void main(String[] args) {

    Double  result;
    Double number1 = 23.0;
    Double number2 = 56.0; 

    System.out.println("Simple Calculator: +, -, * and /");
    // Scanner input = new Scanner(System.in);
    System.out.println("First number is " + number1); 
    // number1 = input.nextDouble();
    System.out.println("Second number is "+ number2);
      // performs addition between numbers
        System.out.print("Addition: ");
        result = number1 + number2;
        System.out.println(number1 + " + " + number2 + " = " + result);
        System.out.println("");
      // performs subtraction between numbers
          System.out.print("Subtraction: ");
        result = number1 - number2;
        System.out.println(number1 + " - " + number2 + " = " + result);
        System.out.println("");
      // performs multiplication between numbers
        System.out.print("Multiplication:");
        result = number1 * number2;
        System.out.println(number1 + " * " + number2 + " = " + result);
        System.out.println("");
      // performs division between numbers
        System.out.print("Division: ");
        result = number1 / number2;
        System.out.println(number1 + " / " + number2 + " = " + result);
  }
}
